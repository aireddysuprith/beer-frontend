import { useEffect, useState } from 'react';
import Modal from './components/Modal';
import useDebounce from './useDebounce';

function App() {

  const [search, setSearch] = useState('');
  const [state, setState] = useState({
    data: null,
    isLoading: false,
    hasError: false
  });
  const [modal, setModal] = useState();
  const [inputs, setInputs] = useState();
  const searchQuery = useDebounce(search, 500);

  const GetAllBeers = async ({q}) => {
    try {
      setState((x) => ({ ...x, data: null, isLoading: true, hasError: false }));
      const query = q ? `?searchText=${q}` : ''
      const data = await fetch(`http://localhost:8080/api/beers/${query}`);
      const json = await data.json();
      setState((x) => ({...x, data: json, isLoading: false, hasError:false}))
    } catch (error) {
      setState((x) => ({ ...x, data: null, isLoading: false, hasError: true }));
    }
  };

  const onChangeSearch = (event) => {
    const value = event.target.value;
    if (!value) {
      setSearch(value);
      return;
    }
    setSearch(value);
  }

  const onChangeInputs = (key) => (event) => {
    setInputs((x) => ({ ...x, [key]: event.target.value }));
  }

  const onSubmitUpdate = async (event) => {
    event.preventDefault();
    const id = event.currentTarget.id.value;
    try {
      const data = await fetch(`http://localhost:8080/api/beers/${id}`, {
        method: 'PUT',
        body: JSON.stringify({
          name: event.currentTarget.name.value,
          description: event.currentTarget.description.value,
          abv: event.currentTarget.abv.value,
          ibu: event.currentTarget.ibu.value
        }),
        headers:  {
          'Content-Type': 'application/json'
        }
      });
      const json = await data.json();
      await GetAllBeers({});
      setModal(undefined);
    } catch (error) {
    }
  }

  const onSubmitCreate = async (event) => {
    event.preventDefault();
    try {
      const data = await fetch(`http://localhost:8080/api/beers`, {
        method: 'POST',
        body: JSON.stringify({
          name: event.currentTarget.name.value,
          description: event.currentTarget.description.value,
          abv: event.currentTarget.abv.value,
          ibu: event.currentTarget.ibu.value
        }),
        headers:  {
          'Content-Type': 'application/json'
        }
      });
      const json = await data.json();
      await GetAllBeers({});
      setModal(undefined);
    } catch (error) {
    }
  }

  const onSubmitDelete = async (event) => {
    event.preventDefault();
    const id = event.currentTarget.id.value;
    try {
        await fetch(`http://localhost:8080/api/beers/${id}`, {
        method: 'DELETE',
        headers:  {
          'Content-Type': 'application/json'
        }
      });
      await GetAllBeers({});
      setModal(undefined);
    } catch (error) {
    }
  }

  useEffect(() => {
    GetAllBeers({});
  }, [])

  useEffect(() => {
    if (state.data && state.isLoading) return;
  }, [state.data]);

  useEffect(() => {
    GetAllBeers({q: searchQuery});
  }, [searchQuery])

  return (
    <div className="App">
      <h1>Beer FrontEnd</h1>
      <main>
        <input placeholder="Search" onChange={onChangeSearch} value={search}
               className="h-10 rounded border border-zinc-300 px-3 mb-4" type="search"/>

        {/*Create Button */}
        <button className="bg-zinc-600 text-white rounded-full px-8" onClick={() => {
          setModal('create');
          setInputs({
            name: '',
            description: '',
            abv: '',
            ibu: ''
          })
        }}>Create
        </button>

        <table className="border border-zinc-200 w-full">
          <tbody>
          <tr className="border-b border-zinc-200">
            <th className="text-left">ID</th>
            <th className="text-left">Name</th>
            <th className="text-left">Description</th>
            <th className="text-left">ABV</th>
            <th className="text-left">IBU</th>
            <th className="text-left">Created At</th>
            <th className="text-left">Actions</th>
          </tr>
          {state.data && state.data.map((i, k) => <tr key={`result-${k}`} className="border-b border-zinc-200">
            <td>{i.id}</td>
            <td className="w-48">{i.name}</td>
            <td className="w-48">{i.description}</td>
            <td>{i.abv}</td>
            <td>{i.ibu}</td>
            <td>{i.createDate.slice(0, 10)}</td>
            <td>
              {/*Edit Button*/}
              <button className="h-6 leading-6 rounded-full block text-white bg-blue-600 px-3" onClick={() => {
                setInputs(i);
                setModal('edit');
              }}>Edit
              </button>

              {/*Delete button*/}
              <button className="h-6 leading-6 rounded-full block text-white bg-red-600 px-3" onClick={() => {
                setInputs(i);
                setModal('delete');
              }}>Delete
              </button>
            </td>
          </tr>)}
          </tbody>
        </table>
      </main>


      {modal !== undefined ? <Modal title={modal} onClose={() => {
        setModal(undefined);
      }}>
        {modal === 'edit' ?
          <form onSubmit={onSubmitUpdate}>
            <div className="block mb-4">
              <input type="hidden" name="id" value={inputs.id} readOnly />
              <input onChange={onChangeInputs('name')} value={inputs.name} className="h-10 border border-zinc-300 px-3" type="text" name="name" />
              <input onChange={onChangeInputs('description')} value={inputs.description} className="h-10 border border-zinc-300 px-3" type="text" name="description" placeholder="Description" />
              <input onChange={onChangeInputs('abv')} value={inputs.abv} className="h-10 border border-zinc-300 px-3" type="number" name="abv" placeholder="ABV" />
              <input onChange={onChangeInputs('ibu')} value={inputs.ibu} className="h-10 border border-zinc-300 px-3" type="number" name="ibu" placeholder="IBU"/>
            </div>
            <div className="block mb-4">
              <button>Submit</button>
            </div>
          </form>: null}

        {modal === 'create' ?
          <form onSubmit={onSubmitCreate}>
            <div className="block mb-4">
              <input type="hidden" name="id" value={inputs} readOnly />
              <input onChange={onChangeInputs('name')} value={inputs.name} className="h-10 border border-zinc-300 px-3" type="text" name="name" placeholder="Name"/>
              <input onChange={onChangeInputs('description')} value={inputs.description} className="h-10 border border-zinc-300 px-3" type="text" name="description" placeholder="Description" />
              <input onChange={onChangeInputs('abv')} value={inputs.abv} className="h-10 border border-zinc-300 px-3" type="number" name="abv" placeholder="ABV" />
              <input onChange={onChangeInputs('ibu')} value={inputs.ibu} className="h-10 border border-zinc-300 px-3" type="number" name="ibu" placeholder="IBU"/>
            </div>
            <div className="block mb-4">
              <button>Submit</button>
            </div>
          </form>: null}

        {modal === 'delete' ?
          <form onSubmit={onSubmitDelete}>
            <div className="block mb-4">
              <input type="hidden" name="id" value={inputs.id} readOnly />
              Are you sure you want to delete this beer?
              <h1>{inputs.name}</h1>
            </div>
            <div className="block mb-4">
              <button className="h-6 leading-6 rounded-full block text-white bg-red-600 px-3">Confirm</button>
            </div>
          </form>: null}
      </Modal> : null}

    </div>
  );
}

export default App;
