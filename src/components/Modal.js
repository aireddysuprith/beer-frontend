import React from 'react';

const Modal = ({title, onClose, children}) => {

  const onClickClose = () => {
    onClose();
  }

  return <div className="fixed inset-0 bg-zinc-600 bg-opacity-50 flex justify-center items-center">
    <div className="bg-white rounded w-full max-w-sm p-8">
      <div className="flex justify-between">
        <h1>{title}</h1>
        <button onClick={onClickClose}>&times;</button>
      </div>
      <main>
        {children}
      </main>
    </div>
  </div>
};

export default Modal;
