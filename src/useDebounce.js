import { useEffect, useState } from "react";

let handler;

const useDebounce = (value, timer) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    clearTimeout(handler);
    handler = setTimeout(() => {
      setDebouncedValue(value);
    }, timer);
    return () => {
      clearTimeout(handler);
    };
  }, [value]);

  return debouncedValue;
}

export default useDebounce;
