# Running app locally

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

1. Start the backend in dev branch using ./gradlew build bootRun
2. In your terminal of the beer-frontend,
    - npm install 
    - npm start
3. Note, to avoid any cors issues, the server is configured to accept any requests from http://localhost:3000
4. http://localhost:3000/

